package com.leadbot.serviceimpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.leadbot.service.AnnualIncomeIntent;

@Service
public class AnnualIncomeIntentImpl implements AnnualIncomeIntent {

	String speech="";
	@Override
	public String annualIncome(Map<String, Map<String, String>> map, String sessionId) {
		speech="It seems Rupees 1 Crore is enough to secure your family's financial future."
				+ " Now need to know bit more about you to provide personalized accurate quote."
				+ " please help me to know your gender.";
		return speech;
	}

}
