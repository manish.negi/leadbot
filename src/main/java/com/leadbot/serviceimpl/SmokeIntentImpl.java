package com.leadbot.serviceimpl;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leadbot.commons.SmokeAPICall;
import com.leadbot.service.SmokeIntent;

@Service
public class SmokeIntentImpl implements SmokeIntent
{
	@Autowired
	private SmokeAPICall smokerAPICall;
	
	String speech="";
	@Override
	public String smoke(Map<String, Map<String, String>> map, String sessionId) 
	{
		if(map.containsKey(sessionId))
		{
			String response = smokerAPICall.smokerAPI(map, sessionId);
			JSONObject object = new JSONObject(response.toString());
			JSONObject res=(JSONObject)object.getJSONObject("response").getJSONObject("payload").getJSONArray("resPlan").get(0);
			String totalPremiumWOGST=res.get("totalPremiumWOGST")+"";

			String policyTerm = res.getString("policyTerm");
			String totalPremium=res.getString("totalPremiumWGST");
			double doubletotalPremiumWGST=Double.parseDouble(totalPremium);

			String convertedtotalPremiumWGST = String.valueOf(doubletotalPremiumWGST);
			String arr [] = convertedtotalPremiumWGST.split("\\.");
			String totalPremiumWGST = arr[0];
			map.get(sessionId).put("policyTerm", policyTerm);
			map.get(sessionId).put("TotalPremiumWOGST", totalPremiumWOGST);
			map.get(sessionId).put("totalPremiumWGST", totalPremium);

			speech="Great Raman Here is your personalised premium quote "
					+ "Your  Sum Assured: Rupees 1 crore."
					+ " Policy Term, "+policyTerm+" years."
					+ " Premium amount: Rupees. "+totalPremiumWGST+" per month inclusive of GST. "
					+ " please tell me your 10 digit mobile number to generate EQuote.";
		}
		else{
			speech="Internal Glitch Please try after some time :-smoker";
		}
		/*try {
		Thread t1=new Thread(new Runnable() 
		{
			public void run() 
			{
				runningBackgroundProcessinsertEkudosValues(
						);
			}
		});
		t1.start();
		t1.join();
		}catch(Exception ex)
		{
			System.out.println("Exception occoured");
		}*/
		return speech;
	}
}
