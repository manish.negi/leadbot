package com.leadbot.serviceimpl;

import org.springframework.stereotype.Service;

import com.leadbot.service.WelcomeYesIntent;

@Service
public class WelcomeYesIntentImpl implements WelcomeYesIntent
{
	String speech="";
	@Override
	public String welcomeYes() {
		speech="I will ask you few Questions to estimate your cover amount. Roughly what is your net annual income.";
		return speech;
	}

}
