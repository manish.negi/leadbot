package com.leadbot.serviceimpl;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leadbot.commons.CreateLeadService;
import com.leadbot.commons.GetLeadService;
import com.leadbot.commons.MailService;
import com.leadbot.service.MobileNumberIntent;

@Service
public class MobileNumberIntentImpl implements MobileNumberIntent 
{
	@Autowired 
	private CreateLeadService createLeadService;
	@Autowired
	private GetLeadService getLeadService;
	@Autowired
	private MailService mailService;
	String speech="";
	@Override
	public String mobileNumber(Map<String, Map<String, String>> map, String sessionId) {
		String response = createLeadService.createLeadAPI(map, sessionId);
		JSONObject object = new JSONObject(response.toString());
		JSONObject res=(JSONObject)object.getJSONObject("response").getJSONObject("responseData").getJSONArray("createLeadWithRiderResponse").get(0);
		String leadId=res.get("leadID")+"";
		map.get(sessionId).put("leadId", leadId);
		String responseleadAPI=getLeadService.getLeadAPI(map, sessionId);
		JSONObject objectlead = new JSONObject(responseleadAPI.toString());
		JSONObject reslead=(JSONObject)object.getJSONObject("response").getJSONObject("responseData").getJSONArray("createLeadWithRiderResponse").get(0);
		//String TotalPremiumWOGST = map.get(sessionId).get("TotalPremiumWOGST");
		JSONObject linkresponse=(JSONObject)objectlead.getJSONObject("response").getJSONObject("responseData").getJSONArray("leadData").get(0);
		String equoteNumber=linkresponse.getJSONObject("lead").get("equoteNumber")+"";
		String link ="https://neouat.maxlifeinsurance.com/campaign-pages/continue-your-journey.html?equoteNumber="+equoteNumber+"";
		speech=" Congratulations! your EQuote number is, " +equoteNumber+
				 " We have sent the details on your E-Mail along with instructions to complete your application, "
				 + " Pleasure talking to you; Greetings from Max Life. ";
		try {
			Thread t1=new Thread(new Runnable() 
			{
				public void run() 
				{
					mailService.getmail("", equoteNumber);
					System.out.println("----Email sent");
				}
			});
			t1.start();
			t1.join();
			
			}
		catch(Exception ex)
		{
			System.out.println();
		}
		return speech;
	}
}

