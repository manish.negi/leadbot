package com.leadbot.serviceimpl;

import org.springframework.stereotype.Service;

import com.leadbot.service.WelcomeIntent;

@Service
public class WelcomeIntentImpl implements WelcomeIntent
{
	String speech="";
	@Override
	public String welcome() {
		speech="Hi Raman, Term Plan provides financial security to your loved one's "
				+ " In a nominal price against odds. If you want, I can help you with a customized quote"
				+ " Would you like to proceed";
		return speech;
	}

}
