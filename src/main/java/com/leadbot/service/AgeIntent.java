package com.leadbot.service;

import java.util.Map;

public interface AgeIntent 
{
	public String age(Map<String, Map<String, String>> map, String sessionId);

}
