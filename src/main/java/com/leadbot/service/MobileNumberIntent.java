package com.leadbot.service;

import java.util.Map;

public interface MobileNumberIntent 
{
	public String mobileNumber(Map<String, Map<String, String>> map, String sessionId);

}
