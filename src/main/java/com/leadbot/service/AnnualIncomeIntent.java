package com.leadbot.service;

import java.util.Map;

public interface AnnualIncomeIntent 
{
	public String annualIncome(Map<String, Map<String, String>> map, String sessionId);

}
