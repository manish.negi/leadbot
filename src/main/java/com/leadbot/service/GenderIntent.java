package com.leadbot.service;

import java.util.Map;

public interface GenderIntent {
  public String gender(Map<String, Map<String, String>> map, String sessionId);
}
