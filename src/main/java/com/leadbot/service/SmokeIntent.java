package com.leadbot.service;

import java.util.Map;

public interface SmokeIntent 
{
	public String smoke(Map<String, Map<String, String>> map, String sessionId);

}
