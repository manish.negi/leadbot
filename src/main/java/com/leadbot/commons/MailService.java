package com.leadbot.commons;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class MailService 
{
	public String getmail(String email ,String equoteNo)
	{
		email="raman.arora@maxlifeinsurance.com";
		String result ="";
		String userDetailURL="https://gatewayuat.maxlifeinsurance.com/apimgm/sb/soa/email/v4?client_id=01004e2d-6d79-4539-883a-294c5663c5bd&client_secret=C2rI6cG7kT4cF5oF0aC6qI7cB3dD1yJ7tL7sR3lC5mX8pO4yG7";
		try{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-IBM-Client-Id", "01004e2d-6d79-4539-883a-294c5663c5bd");
		headers.set("X-IBM-Client-Secret", "C2rI6cG7kT4cF5oF0aC6qI7cB3dD1yJ7tL7sR3lC5mX8pO4yG7");
		StringBuilder sb=new StringBuilder();
		sb.append(" 	{	 ");
		sb.append(" 	  \"request\": {	 ");
		sb.append(" 	    \"header\": {	 ");
		sb.append(" 	      \"soaCorrelationId\": \"null\",");
		sb.append(" 	      \"soaAppId\": \"POS\"	 ");
		sb.append(" 	    },	 ");
		sb.append(" 	    \"requestData\": {	 ");
		sb.append(" 	      \"mailIdTo\": \"" +email+"\"," );
		sb.append(" 	      \"mailSubject\": \"Equote number for you online tem plan\",");
		sb.append(" 	      \"fromName\": \"Maxlife \",");
		sb.append(" 	      \"attachments\": [],");
		sb.append(" 	      \"isConsolidate\": false,");
		sb.append(" 	      \"isFileAttached\": true,");
		sb.append(" 	      \"fromEmail\": \"DoNotReply@maxlifeinsurance.com\",");
		sb.append(" 	      \"mailBody\": \"<html> <body> Dear Customer, <br/> <br/> Thumbs up for taking the first step in securing your family. <br/> Your Max Life Equote no. "+equoteNo+". <br/> Make your next move and complete the purchase now <a href=https://goo.gl/cpkEmY>https://goo.gl/cpkEmY</a> <br/><br/> Yours sincerely, <br/> Online Customer Care Team <br/> Max Life Insurance <br/> <br/> MAX LIFE INSURANCE CO. LTD. <br/> 3rd, 11th and 12th Floor, DLF Square, Jacaranda Marg, DLF City Phase II, Gurugram, Haryana - 122 002, India. <br/> T +91-124-4159300 F +91-124-4159399 www.maxlifeinsurance.com <br/> Registered Office: Max House, 3rd Floor, 1 DrJhaMarg, Okhla, New Delhi 110 020, India. <br/> </body> </html>\"");
		sb.append(" 	    }	 ");
		sb.append(" 	    }	 ");
		sb.append(" 	    }	 ");
		
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(userDetailURL, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200){
			 result =response.getBody();
		}else{
			System.out.println("Error While getting User Details through SOA API");
		}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
