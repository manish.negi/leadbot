package com.leadbot.commons;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;
@Service
public class SmokeAPICall 
{
	public String smokerAPI(Map<String, Map<String, String>> map, String sessionId) {
		System.out.println("SMOKER API : - Inside Smoker API:: START");
		StringBuilder result = new StringBuilder();
		String output = new String();
		//ResourceBundle res = ResourceBundle.getBundle("application");
		String soaCorrelationId = "CorelationId"+System.currentTimeMillis();
		HttpURLConnection conn = null;
		try {
			String age = map.get(sessionId).get("age");
			String policyTerm="10";

			String gender=map.get(sessionId).get("gender");
			String smoker=map.get(sessionId).get("smoke");
			String extURL = "https://gatewayuat.maxlifeinsurance.com/apimgm/sb/soa/nb/quotesnillustrations/productpremium/v1?client_id=8ffc6920-d69d-4638-8abc-87328321302b&client_secret=hL6mM3jU7iO4wG2wW6fF6kC0qR6jY0qI1qP0jR3bK3cR1eU0tN";
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();
			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaCorrelationId\": \""+soaCorrelationId+"\",	");
			requestdata.append("	      \"soaAppId\": \"NEO\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"payload\": {	");
			requestdata.append("	      \"reqPlan\": [	");
			requestdata.append("	        {	");
			requestdata.append("	          \"planId\": \"TCOTP2\",	");
			requestdata.append("	          \"variantId\": \"SA\",	");
			requestdata.append("	          \"gender\": \""+gender+"\",	");
			requestdata.append("	          \"age\": \""+age+"\",	");
			requestdata.append("	          \"empDiscount\": \"N\",	");
			requestdata.append("	          \"planSumAssured\": \"10000000\",	");
			requestdata.append("	          \"policyTerm\": \""+policyTerm+"\",	");
			requestdata.append("	          \"policyPayTerm\": \""+policyTerm+"\",	");
			requestdata.append("	          \"smoke\": \""+smoker+"\",	");
			requestdata.append("	          \"mode\": \"Monthly\",	");
			requestdata.append("	          \"reqRider\": [	");
			requestdata.append("	          	");
			requestdata.append("	          ]	");
			requestdata.append("	        }	");
			requestdata.append("	      ]	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");
			System.out.println("SMOKER API REQUEST -- "+requestdata.toString());
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		return result.toString();

	}
}
