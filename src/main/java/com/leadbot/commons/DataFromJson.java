package com.leadbot.commons;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DataFromJson 
{
	String annualIncome="", gender="", custEmail="", mobileNum="", age="", smoke="" ;

	public String customerNameVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		
		Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		
		if(externalMap.containsKey(sessionId))
		{
			try {
				annualIncome = object.getJSONObject("result").getJSONObject("parameters").get("annualIncome")+"";
				externalMap.get(sessionId).put("annualIncome", annualIncome);
			} catch (Exception e) {
				annualIncome = "";
			}
		}
		else
		{
			try {
				annualIncome = object.getJSONObject("result").getJSONObject("parameters").get("annualIncome")+"";
				internalMap.put("name", "Raman");
				externalMap.put(sessionId, internalMap);
				externalMap.get(sessionId).put("annualIncome", annualIncome);
			} catch (Exception e) {
				annualIncome = "";
			}
		}
		return annualIncome;
	}
	public String genderVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				gender = object.getJSONObject("result").getJSONObject("parameters").get("gender")+"";
				if("male".equalsIgnoreCase(gender)){
					gender="M";
					externalMap.get(sessionId).put("gender", gender);
				}
				else{
					gender="F";
					externalMap.get(sessionId).put("gender", gender);
				}
			} catch (Exception e) {
				gender = "";
			}
		}
		else
		{
			try {

				gender = object.getJSONObject("result").getJSONObject("parameters").get("gender")+"";
				if("male".equalsIgnoreCase(gender)){
					gender="M";
					externalMap.get(sessionId).put("gender", gender);
				}
				else{
					gender="F";
					externalMap.get(sessionId).put("gender", gender);
				}
			} catch (Exception e) {
				gender = "";
			}
		}
		return gender;
	}
	public String email_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				custEmail = object.getJSONObject("result").getJSONObject("parameters").get("email")+"";
				custEmail=custEmail.replaceAll("\\s","");
				externalMap.get(sessionId).put("custEmail", custEmail);
			} catch (Exception e) {
				custEmail = "";
			}
		}
		else
		{
			try {
				custEmail = object.getJSONObject("result").getJSONObject("parameters").get("email")+"";
				custEmail=custEmail.replaceAll("\\s","");
				externalMap.get(sessionId).put("custEmail", custEmail);
			} catch (Exception e) {
				custEmail = "";
			}
		}
		return custEmail;
	}
	public String mobile_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				mobileNum = object.getJSONObject("result").getJSONObject("parameters").get("mobilenum")+"";
				externalMap.get(sessionId).put("mobileNum", mobileNum);
			} catch (Exception e) {
				mobileNum = "";
			}
		}
		else
		{
			try {
				mobileNum = object.getJSONObject("result").getJSONObject("parameters").get("mobilenum")+"";
				externalMap.get(sessionId).put("mobileNum", mobileNum);
			} catch (Exception e) {
				mobileNum = "";
			}
		}
		return mobileNum;
	}
	public String date_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			try {
				age = object.getJSONObject("result").getJSONObject("parameters").get("age")+"";
				externalMap.get(sessionId).put("age", age);
			} catch (Exception e) {
				age = "";
			}
		}
		else
		{
			try {
				age = object.getJSONObject("result").getJSONObject("parameters").get("age")+"";
			} catch (Exception e) {
				age = "";
			}
		}
		return age;
	}
	public String smoke_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			try {
				smoke = object.getJSONObject("result").getJSONObject("parameters").get("smoke")+"";
				if("smoker".equalsIgnoreCase(smoke) || "Yes".equalsIgnoreCase(smoke) || "smoke".equalsIgnoreCase(smoke)){
					smoke="Y";
					externalMap.get(sessionId).put("smoke", smoke);
				}
				else{
					smoke="N";
					externalMap.get(sessionId).put("smoke", smoke);
				}
			} catch (Exception e) {
				smoke = "";
			}
		}
		else
		{
			try {
				smoke = object.getJSONObject("result").getJSONObject("parameters").get("smoke")+"";
			} catch (Exception e) {
				smoke = "";
			}
		}
		return smoke;
	}
}
