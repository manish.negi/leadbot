package com.leadbot.commons;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;

@Service
public class GetLeadService 
{
	public String getLeadAPI(Map<String, Map<String, String>> map, String sessionId) {

		StringBuilder result = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try {
			String leadIds=map.get(sessionId).get("leadId");
			System.out.println("Get Lead API Lead Id- "+leadIds);
			int leadId = Integer.parseInt(leadIds);
			String extURL = "https://gatewayuat.maxlifeinsurance.com/apimgm/sb/soa/getlead/v8";
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();
			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaUserId\": \"NEO123\",	");
			requestdata.append("	      \"soaCorrelationId\": \"25478965874\",	");
			requestdata.append("	      \"soaPassword\": \"bmVvQDEyMw==\",	");
			requestdata.append("	      \"soaMsgVersion\": \"1.0\",	");
			requestdata.append("	      \"soaAppId\": \"NEO\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"requestData\": {	");
			requestdata.append("	      \"getLead\": {	");
			requestdata.append("	        \"leadId\": "+leadId+"	");
			requestdata.append("	      }	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("Get Lead API Calling Response Code:- "+apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		return result.toString();
	}
}
