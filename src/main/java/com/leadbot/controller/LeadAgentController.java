package com.leadbot.controller;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leadbot.button.InnerData;
import com.leadbot.commons.DataFromJson;
import com.leadbot.response.WebhookResponse;
import com.leadbot.service.AgeIntent;
import com.leadbot.service.AnnualIncomeIntent;
import com.leadbot.service.GenderIntent;
import com.leadbot.service.MobileNumberIntent;
import com.leadbot.service.SmokeIntent;
import com.leadbot.service.WelcomeIntent;
import com.leadbot.service.WelcomeYesIntent;

@RestController
@RequestMapping("/leadConversation")
public class LeadAgentController 
{
	@Autowired
	private DataFromJson dataFromJson;
	@Autowired
	private WelcomeIntent welcomeIntent;
	@Autowired
	private WelcomeYesIntent welcomeYesIntent;
	@Autowired
	private AnnualIncomeIntent annualIncomeIntent;
	@Autowired
	private GenderIntent genderIntent;
	@Autowired
	private AgeIntent ageIntent;
	@Autowired
	private SmokeIntent smokeIntent;
	@Autowired
	private MobileNumberIntent mobileNumberIntent;



	static Map<String,Map<String,String>> ExternalMap = new ConcurrentHashMap<String,Map<String,String>>();
	Map<String,String> internalMap = new ConcurrentHashMap<String,String>();

	@RequestMapping(method = RequestMethod.POST)
	/*public WebhookResponse webhook(@RequestBody String obj) {*/
	public @ResponseBody String webhook(@RequestBody String obj) {
		System.out.println("Testing :-"+obj);
		InnerData innerData = new InnerData();
		System.out.println("Inside Controller");
		System.out.println(obj.toString());
		String speech="", sessionId = "", action="", customerName="";
		String gender="", custEmail="", mobileNum="", date="", smoke="";
		try {
			System.out.println("Inside Try.........");
			JSONObject object = new JSONObject(obj.toString());
			System.out.println("Request	 "+obj.toString());
			sessionId = object.get("session")+"";
			System.out.println("--------------testing Manish");
			action = object.getJSONObject("queryResult").get("action")+"";
			System.out.println("Action----"+action);

			/*customerName=dataFromJson.customerNameVariable(object,sessionId, ExternalMap);
			gender=dataFromJson.genderVariable(object,sessionId, ExternalMap);
			custEmail=dataFromJson.email_Variable(object, sessionId, ExternalMap);
			mobileNum=dataFromJson.mobile_Variable(object, sessionId, ExternalMap);
			date=dataFromJson.date_Variable(object, sessionId, ExternalMap);
			smoke=dataFromJson.smoke_Variable(object, sessionId, ExternalMap);*/

			/*switch(action.toUpperCase())
			{
			//Welcome 1
			case "WELCOME":
			{
				//speech=welcomeIntent.welcome();
				speech="";
			}
			break;
			case "DEFAULTWELCOMEINTENT.DEFAULTWELCOMEINTENT-YES":
			{
				speech=welcomeYesIntent.welcomeYes();
			}
			break;
			case "DEFAULTWELCOMEINTENT.DEFAULTWELCOMEINTENT-YES.DEFAULTWELCOMEINTENT-YES-CUSTOM":
			{
				speech=annualIncomeIntent.annualIncome(ExternalMap, sessionId);
			}
			break;
			case "GENDER":
			{
				speech=genderIntent.gender(ExternalMap, sessionId);
			}
			break;
			case "DOB":
			{
				speech=ageIntent.age(ExternalMap, sessionId);
			}
			break;
			case "SMOKE":
			{
				speech=smokeIntent.smoke(ExternalMap, sessionId);
			}
			break;
			case "MOBILENUMBER":
			{
				speech=mobileNumberIntent.mobileNumber(ExternalMap, sessionId);

			}
			break;
			default : 
			{
				System.out.println("Intent Not match with skill, Please connect to application owner");
				speech="Intent Not match with skill, Please connect to application owner";
			}
			}*/
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured");
			speech="Communication glitch while calling API's.";
		}
		System.out.println("Final Speech--"+speech);
		WebhookResponse responseObj = new WebhookResponse(speech, speech, innerData);
		StringBuffer sb = new StringBuffer();


		//Simple Response 
		System.out.println("--------------"+action);
		if("welcome".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"This is an Suggestion Chip Response for GoogleAssistance\"	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ],	");
			sb.append("	                    \"suggestions\": [	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"CarouselBrowse\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"BasicCard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"OtherCard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"Never mind\"	");
			sb.append("	                        }	");
			sb.append("	                    ]	");
			sb.append("	                }	");
			sb.append("	    },	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"text\": \"Hello, Facebook!\"	");
			sb.append("	    },	");
			sb.append("	    \"slack\": {	");
			sb.append("	      \"text\": \"This is a text response for Slack.\"	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");

		}
		else if("carouselBrowse".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"This is an CarouselBrowse Response for GoogleAssistance\"	");
			sb.append("	            }	");
			sb.append("	          },	");
			sb.append("	          {	");
			sb.append("	            \"carouselBrowse\": {	");
			sb.append("	              \"items\": [	");
			sb.append("	                {	");
			sb.append("	                  \"title\": \"Title of item 1\",	");
			sb.append("	                  \"description\": \"Description of item 1\",	");
			sb.append("	                  \"footer\": \"Item 1 footer\",	");
			sb.append("	                  \"image\": {	");
			sb.append("	                    \"url\": \"https://i.cartoonnetwork.com/prismo/props/chars/ben17_180x180_0.png\",	");
			sb.append("	                    \"accessibilityText\": \"Google Assistant Bubbles\"	");
			sb.append("	                  },	");
			sb.append("	                  \"openUrlAction\": {	");
			sb.append("	                    \"url\": \"https://google.com\"	");
			sb.append("	                  }	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"title\": \"Title of item 2\",	");
			sb.append("	                  \"description\": \"Description of item 2\",	");
			sb.append("	                  \"footer\": \"Item 2 footer\",	");
			sb.append("	                  \"image\": {	");
			sb.append("	                    \"url\": \"https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX23904753.jpg\",	");
			sb.append("	                    \"accessibilityText\": \"Google Assistant Bubbles\"	");
			sb.append("	                  },	");
			sb.append("	                  \"openUrlAction\": {	");
			sb.append("	                    \"url\": \"https://yahoo.com\"	");
			sb.append("	                  }	");
			sb.append("	                }	");
			sb.append("	              ]	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ]	");
			sb.append("	      }	");
			sb.append("	    },	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"text\": \"Hello, Facebook!\"	");
			sb.append("	    },	");
			sb.append("	    \"slack\": {	");
			sb.append("	      \"text\": \"This is a text response for Slack.\"	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}
		else if("BasicCard".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"This is an Basic Card rich response\"	");
			sb.append("	            }	");
			sb.append("	          },	");
			sb.append("	                        {	");
			sb.append("	                            \"basicCard\": {	");
			sb.append("	                                \"title\": \"Google Pay\",	");
			sb.append("	                                \"formattedText\": \"G Pay is a digital wallet.\",	");
			sb.append("	                                \"image\": {	");
			sb.append("	                                    \"url\": \"https://pay.google.com/about/static/images/social/og_image.jpg\",	");
			sb.append("	                                    \"accessibilityText\": \"Image alternate text\"	");
			sb.append("	                                },	");
			sb.append("	                                \"buttons\": [	");
			sb.append("	                                    {	");
			sb.append("	                                        \"title\": \"To know Read more\",	");
			sb.append("	                                        \"openUrlAction\": {	");
			sb.append("	                                            \"url\": \"https://pay.google.com/payments/u/0/home\"	");
			sb.append("	                                        }	");
			sb.append("	                                    }	");
			sb.append("	                                ],	");
			sb.append("	                                \"imageDisplayOptions\": \"CROPPED\"	");
			sb.append("	                            }	");
			sb.append("	                        }	");
			sb.append("	        ],	");
			sb.append("	                    \"suggestions\": [	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"CarouselBrowse\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"BasicCard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"Tablecard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"TableCardComplex\"	");
			sb.append("	                        }	");
			sb.append("	                    ]	");
			sb.append("	      }	");
			sb.append("	    },	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"text\": \"Hello, Facebook!\"	");
			sb.append("	    },	");
			sb.append("	    \"slack\": {	");
			sb.append("	      \"text\": \"This is a text response for Slack...\"	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}
		else if("Tablecard".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"This is an Simple TableCard Response for GoogleAssistance\"	");
			sb.append("	            }	");
			sb.append("	          },	");
			sb.append("	 {	");
			sb.append("	            \"tableCard\": {	");
			sb.append("	              \"rows\": [	");
			sb.append("	                {	");
			sb.append("	                  \"cells\": [	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 1\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 2\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 3\"	");
			sb.append("	                    }	");
			sb.append("	                  ],	");
			sb.append("	                  \"dividerAfter\": true	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"cells\": [	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 1\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 2\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 3\"	");
			sb.append("	                    }	");
			sb.append("	                  ],	");
			sb.append("	                  \"dividerAfter\": true	");
			sb.append("	                }	");
			sb.append("	              ],	");
			sb.append("	              \"columnProperties\": [	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 1\"	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 2\"	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 3\"	");
			sb.append("	                }	");
			sb.append("	              ]	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ]	");
			sb.append("	      }	");
			sb.append("	    },	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"text\": \"Hello, Facebook!\"	");
			sb.append("	    },	");
			sb.append("	    \"slack\": {	");
			sb.append("	      \"text\": \"This is a text response for Slack.\"	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}
		else if("TableCardComplex".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"This is an Complex Table Card Response for GoogleAssistance\"	");
			sb.append("	            }	");
			sb.append("	          },	");
			sb.append("	          {	");
			sb.append("	            \"tableCard\": {	");
			sb.append("	              \"title\": \"Table Title\",	");
			sb.append("	              \"subtitle\": \"Table Subtitle\",	");
			sb.append("	              \"image\": {	");
			sb.append("	                \"url\": \"https://avatars0.githubusercontent.com/u/23533486\",	");
			sb.append("	                \"accessibilityText\": \"Actions on Google\"	");
			sb.append("	              },	");
			sb.append("	              \"rows\": [	");
			sb.append("	                {	");
			sb.append("	                  \"cells\": [	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 1\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 2\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 1 item 3\"	");
			sb.append("	                    }	");
			sb.append("	                  ],	");
			sb.append("	                  \"dividerAfter\": false	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"cells\": [	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 1\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 2\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 3\"	");
			sb.append("	                    }	");
			sb.append("	                  ],	");
			sb.append("	                  \"dividerAfter\": true	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"cells\": [	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 1\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 2\"	");
			sb.append("	                    },	");
			sb.append("	                    {	");
			sb.append("	                      \"text\": \"row 2 item 3\"	");
			sb.append("	                    }	");
			sb.append("	                  ]	");
			sb.append("	                }	");
			sb.append("	              ],	");
			sb.append("	              \"columnProperties\": [	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 1\",	");
			sb.append("	                  \"horizontalAlignment\": \"CENTER\"	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 2\",	");
			sb.append("	                  \"horizontalAlignment\": \"LEADING\"	");
			sb.append("	                },	");
			sb.append("	                {	");
			sb.append("	                  \"header\": \"header 3\",	");
			sb.append("	                  \"horizontalAlignment\": \"TRAILING\"	");
			sb.append("	                }	");
			sb.append("	              ],	");
			sb.append("	              \"buttons\": [	");
			sb.append("	                {	");
			sb.append("	                  \"title\": \"Button Title\",	");
			sb.append("	                  \"openUrlAction\": {	");
			sb.append("	                    \"url\": \"https://github.com/actions-on-google\"	");
			sb.append("	                  }	");
			sb.append("	                }	");
			sb.append("	              ]	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ]	");
			sb.append("	      },	");
			sb.append("	      \"facebook\": {	");
			sb.append("	        \"text\": \"Hello, Facebook!\"	");
			sb.append("	      },	");
			sb.append("	      \"slack\": {	");
			sb.append("	        \"text\": \"This is a text response for Slack.\"	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}	
		else if("input.list".equalsIgnoreCase(action))
		{
			/*sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"Choose a item\"	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ]	");
			sb.append("	      },	");
			sb.append("	      \"systemIntent\": {	");
			sb.append("	        \"intent\": \"actions.intent.OPTION\",	");
			sb.append("	        \"data\": {	");
			sb.append("	          \"@type\": \"type.googleapis.com/google.actions.v2.OptionValueSpec\",	");
			sb.append("	          \"listSelect\": {	");
			sb.append("	            \"title\": \"Hello\",	");
			sb.append("	            \"items\": [	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"first title key\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			//sb.append("	                  \"url\": \"https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"first title\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"second\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"second description\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"https://lh3.googleusercontent.com/Nu3a6F80WfixUqf_ec_vgXy_c0-0r4VLJRXjVFF_X_CIilEu8B9fT35qyTEj_PEsKw\",	");
			sb.append("	                  \"accessibilityText\": \"secondtext\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"second title\"	");
			sb.append("	              }	");
			sb.append("	            ]	");
			sb.append("	          }	");
			sb.append("	        }	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");*/
			
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"Choose a item\"	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ]	");
			sb.append("	      },	");
			sb.append("	      \"systemIntent\": {	");
			sb.append("	        \"intent\": \"actions.intent.OPTION\",	");
			sb.append("	        \"data\": {	");
			sb.append("	          \"@type\": \"type.googleapis.com/google.actions.v2.OptionValueSpec\",	");
			sb.append("	          \"listSelect\": {	");
			sb.append("	            \"title\": \"Questions\",	");
			sb.append("	            \"items\": [	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"firstkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 1\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"Secondkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 2\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"Thirdkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 3\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"forthkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 4\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"fifthkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 5\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"sixthkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"\",	");
			sb.append("	                  \"accessibilityText\": \"first alt\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 6\"	");
			sb.append("	              },	");
			sb.append("	              {	");
			sb.append("	                \"optionInfo\": {	");
			sb.append("	                  \"key\": \"seventhkey\"	");
			sb.append("	                },	");
			sb.append("	                \"description\": \"\",	");
			sb.append("	                \"image\": {	");
			sb.append("	                  \"url\": \"https://lh3.googleusercontent.com/Nu3a6F80WfixUqf_ec_vgXy_c0-0r4VLJRXjVFF_X_CIilEu8B9fT35qyTEj_PEsKw\",	");
			sb.append("	                  \"accessibilityText\": \"secondtext\"	");
			sb.append("	                },	");
			sb.append("	                \"title\": \"Question 7\"	");
			sb.append("	              }	");
			sb.append("	            ]	");
			sb.append("	          }	");
			sb.append("	        }	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}
		else if("input.unknown".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"google\": {	");
			sb.append("	      \"expectUserResponse\": true,	");
			sb.append("	      \"richResponse\": {	");
			sb.append("	        \"items\": [	");
			sb.append("	          {	");
			sb.append("	            \"simpleResponse\": {	");
			sb.append("	              \"textToSpeech\": \"Finally I got that Response from GoogleAssistance\"	");
			sb.append("	            }	");
			sb.append("	          }	");
			sb.append("	        ],	");
			sb.append("	                    \"suggestions\": [	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"CarouselBrowse\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"BasicCard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"OtherCard\"	");
			sb.append("	                        },	");
			sb.append("	                        {	");
			sb.append("	                            \"title\": \"Never mind\"	");
			sb.append("	                        }	");
			sb.append("	                    ]	");
			sb.append("	                }	");
			sb.append("	    },	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"text\": \"Hello, Facebook!\"	");
			sb.append("	    },	");
			sb.append("	    \"slack\": {	");
			sb.append("	      \"text\": \"This is a text response for Slack.\"	");
			sb.append("	    }	");
			sb.append("	  }	");
			sb.append("	}	");
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

}
